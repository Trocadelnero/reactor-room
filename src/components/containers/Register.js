import React, { Fragment } from "react";
import RegisterForm from "../forms/RegisterForm";
import { Link, useHistory } from "react-router-dom";

const Register = () => {

const history = useHistory();

const handleRegisterComplete = (result) => {
  console.log('Triggered from RegisterForm', result);
  if(result) {
    history.replace("/dashboard");
  }
  
};
return (
  
   <Fragment>
        <h1>Register to Survey Puppy</h1>
        <RegisterForm complete={ handleRegisterComplete } />

        <Link to="/login">
      Already Registered?
        </Link>
      </Fragment>
);
};
export default Register;