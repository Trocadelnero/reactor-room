import React, {Fragment} from "react";
import LoginForm from "../forms/LoginForm";
import { Link, useHistory } from "react-router-dom";


const Login = () => {

  const history = useHistory();

  const handleLoginComplete = (result) => {
    console.log('Triggered from LoginForm', result);
    if(result) {
      history.push("/dashboard");
    }
    
  };

  return (
   
    <Fragment>
      <h1> Login to survey puppy</h1>
      
      <LoginForm complete={ handleLoginComplete }/>
      <Link to="/register">Register Here!</Link>
</Fragment>
  );
};

export default Login;
