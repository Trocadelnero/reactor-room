import React from 'react';
import { useHistory } from 'react-router-dom';

const Home = props =>  {

//Programatically redir
    const history = useHistory();

 const OnGoHomeClicked = () => {
     history.replace("/dashboard");
 };
    return (
        <div>
            <h1>Welcome Home</h1>
            <button onClick={OnGoHomeClicked} >Go to Dashboard</button>
        </div>
    
)
    }

    export default Home;