import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = props =>  (
      <React.Fragment>
          <br/>
        <img src="https://image.freepik.com/free-vector/404-error-with-glitch-effect_225004-656.jpg" width="404" alt="Not Found" />
        <br/>
      <Link to="/home">Go back Home</Link>
        <br/>
        <a href="https://www.freepik.com/free-photos-vectors/technology">Technology vector created by dgim-studio - www.freepik.com</a>
      </React.Fragment>
);

    export default NotFound;