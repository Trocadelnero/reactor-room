import React, { useState, Fragment } from "react";
import { loginUser } from "../../api/user.api";



const LoginForm = props => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [loginError, setLoginError] = useState("");

  const onLoginClicked = async (ev) => {
    setIsLoading(true);
let result;
    try {
      const {status} = await loginUser(username, password);
     result = status === 200;
    } catch (e) {
      setLoginError(e.message || e);
    } finally {
      setIsLoading(false);
      props.complete(result);
    }
  };

  const onUsernameChanged = (ev) => setUsername(ev.target.value);
  const onPasswordChanged = (ev) => setPassword(ev.target.value);

  return (
    <form>
       <Fragment>
        <label>username</label>
        <input
         type="text"
          placeholder="Enter your username"
          onChange={onUsernameChanged}
          />
      </Fragment>

      <Fragment>
        <label>password</label>
        <input type="password"
         placeholder="Enter your password"
         onChange={onPasswordChanged}
         />
      </Fragment>

      <Fragment>
        <button type="button" onClick={onLoginClicked}>
          Login
        </button>
        </Fragment>

      { isLoading && <div>Attempting Login...</div> }
  { loginError && <div>{ loginError }</div> }

    </form>
  );
};

export default LoginForm;
