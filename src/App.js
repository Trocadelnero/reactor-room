import React from "react";
import { BrowserRouter as Router,
Switch,
Route, 
NavLink,
Redirect} from "react-router-dom";
import "./App.css";
import Home from "./components/containers/Home";
import Dashboard from "./components/containers/Dashboard";
import Register from "./components/containers/Register";
import Login from "./components/containers/Login";
import Subscribe from "./components/containers/Subscribe";
import Contact from "./components/containers/Contact";
import NotFound from "./components/containers/NotFound";

function App() {
  return (
    <Router>
      <div>
        My first Reaction


    <nav>
      <ul>
        <NavLink to="/home">Home</NavLink>
        <NavLink to="/dashboard">Dashboard</NavLink>
        <NavLink to="/contact">Contact</NavLink>
        <NavLink to="/subscribe">Subscribe</NavLink>
        <NavLink to="/login">Login</NavLink>
        <NavLink to="/register">Register</NavLink>

      </ul>
    </nav>


      <Switch>
      <Route exact path="/"><Redirect to="/login"></Redirect></Route>
      <Route path="/home" component={ Home } />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/subscribe" component={Subscribe} />
        <Route path="/contact" component={Contact} />
        <Route path="*" component={NotFound} />
      </Switch>
       
       
      </div>
    </Router>
  );
}

export default App;
