const API_REGISTER_URL = 'https://survey-poodle.herokuapp.com/v1/api/users/register';

const API_LOGIN_URL = 'https://survey-poodle.herokuapp.com/v1/api/users/login';

const createFetchOptions = (method, body) => ({
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  });

export const registerUser = (username, password) => {
    return fetch(
        API_REGISTER_URL,
        createFetchOptions('POST', {
            user: {
                username,
                password
            }
        })
        ).then(r => r.json())
        .then(response => {
            if (response.status >= 400) {
                throw Error(response.error);
                
            }
            return response;
        })
    }

    //trial

    export const loginUser = (username, password) => {
        return fetch(
            API_LOGIN_URL,
            createFetchOptions('POST', {
                user: {
                    username,
                    password
                }
            })
            ).then(r => r.json())
            .then(response => {
                if (response.status >= 400) {
                    throw Error(response.error);
                    
                }
                return response;
            })
        }
