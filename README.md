# Reactor-Room

## Description
**Playground for Learning [React](https://reactjs.org/)**.\
Implementing from Lessons learned through a React Video-series.\
Completed up to [13] from the series. \
Plan is to later expand or branch this project with own ideas.\

## App Includes, so far:
**/api**
1. user.api.js
**/components**
- containers
1. Contact.js
2. Dashboard.js
3. Home.js
4. Login.js
5. NotFound.js
6. Register.js
7. Subscribe.js
- presentational
1. DashboardMessage.js
- forms
1. LoginForm.js
2. RegisterForm.js

**App.css**
- green/yellow theme with focus on borderstyles around elements.
- Still learning how to implement correctly/efficiently.
- Personal theme *under development*, used in various projects.



This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).\

## Available Scripts
In the project directory, you can run:
### `npm install`
Install dependencies e.g if you cloned this project.

### `npm start`
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).

### Making a Progressive Web App
This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration
This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration